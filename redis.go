package main

import (
	"sync"

	"github.com/go-redis/redis"
)

var (
	once     sync.Once
	instance *redis.Client
)

type RedisConfig struct {
	Host    string `json:"RedisHost"`
	Port    string `json:"RedisPort"`
	Pass    string `json:"RedisPass"`
	DbIndex int    `json:"RedisDbIndex"`
}

func GetClient() *redis.Client {
	once.Do(func() {
		redisConfig := &RedisConfig{
			Host:    "8.129.10.229",
			Port:    "6379",
			Pass:    "nE7jA%5m",
			DbIndex: 0,
		}
		instance = newRedisClient(redisConfig)
	})

	return instance
}

func newRedisClient(redisCfg *RedisConfig) *redis.Client {
	addr := redisCfg.Host + ":" + redisCfg.Port
	db := redisCfg.DbIndex
	RedisPass := redisCfg.Pass

	return redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: RedisPass,
		DB:       db,
	})
}
