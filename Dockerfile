FROM golang:latest as builder

# $GOPATH = /go
WORKDIR /go/src/lobby
ENV GO15VENDOREXPERIMENT 1
COPY . .
#RUN go get github.com/astaxie/beego && go get github.com/go-sql-driver/mysql && \
#    go get github.com/gomodule/redigo/redis && \
#    go get github.com/jinzhu/gorm

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o lobby .

FROM alpine:3.11
#ENV ENV_REACTOR test1
WORKDIR /
COPY --from=builder /go/src/lobby/lobby .
CMD ["./lobby"]
