package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"hash/crc32"
	"net/http"
	"os"
)

func GetIndex(s string) int {
	return int(crc32.ChecksumIEEE([]byte(s))) % 10
}

func index(c *gin.Context) {
	str := c.DefaultQuery("str", "")
	fmt.Printf("get str:%v\n", str)
	idx := GetIndex(str)
	c.String(http.StatusOK, fmt.Sprintf("%v", idx))
}

func main() {
	port := "6601"
	mname := os.Getenv("SERVER_NAME")
	mip := os.Getenv("SERVER_IP")
	lobbyinfo := mname + "|" + mip + ":" + port
	GetClient().Set("LobbyInfo", lobbyinfo, 0)
	fmt.Printf("lobby info %v\n", lobbyinfo)

	gin.SetMode(gin.DebugMode)
	router := gin.Default()
	router.GET("/idx", index)

	router.Run(":" + port)
}
